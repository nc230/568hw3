#include <stdio.h>
#include <stdlib.h>

void function(int a, int b, int c) {
  char buffer1[5];
  char buffer2[20];
  buffer1[0] = 'h';
  buffer2[0] = 'i';
  buffer1[4] = 'j';
  buffer2[19] = 'k';
  int * ret;
  ret = (int *)(buffer1 + 21);
  (*ret) += 7;
}

void main() {
  int x;
  x = 0;
  function(1, 2, 3);
  x = 1;
  printf("x=%d\n", x);
}
